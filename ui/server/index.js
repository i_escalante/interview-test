'use strict'

const
    http = require('http'),
    express = require('express'),
    morgan = require('morgan'),
    path = require('path'),
    bodyParser = require('body-parser'),
    rp = require('request-promise')


const HTTP_PORT = process.env.HTTP_PORT || 80;
const API_BASE_URL = process.env.API_BASE_URL || 'http://api';
const DIR_NAME =  __dirname

const app = express()

// Setup logger
app.use(morgan(':remote-addr - :remote-user [:date[clf]] ":method :url HTTP/:http-version" :status :res[content-length] :response-time ms'))

app.get('*.js', function (req, res, next) {
    req.url = req.url + '.gz'
    res.set('Content-Encoding', 'gzip')
    res.set('Content-Type', 'application/javascript')
    next()
})

// Static assets
app.use(express.static(path.resolve(DIR_NAME, '..', 'public')))

app.use(bodyParser.json({limit: '5mb'}))
app.use(bodyParser.urlencoded({limit: '100mb'}))

app.use('/api', (req, res) => {
    let request = {
        method: req.method,
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
        },
        uri: API_BASE_URL + req.url.replace('/api', ''),
        resolveWithFullResponse: true,
        json: true
    }
    if (req.body && Object.keys(req.body).length !== 0) {
        req.body = req.body
    }
    rp(request).then(apiResponse => {
        for (let header in apiResponse.headers) {
            res.set(header, apiResponse.headers[header])
        }
        res.status(apiResponse.statusCode || 500).send(apiResponse.body)
    }).catch(err => {
        res.status(err.statusCode).send(err.error)
    })
})

app.get('*', (req, res) => {
    res.sendFile(path.resolve(DIR_NAME, '../public/index.html'))
})

const httpServer = http.createServer(app)

httpServer.listen(HTTP_PORT, () => {
    console.log(`App listening on port ${HTTP_PORT}!`);
})
