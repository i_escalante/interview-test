import Search from './search/listing.component.jsx'

const AppRoutes = [
    {
        path: '/',
        main: Search,
        exact: true,
    },
]

export default AppRoutes
