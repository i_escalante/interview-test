import React, {Component} from 'react'
import {IntlProvider, addLocaleData} from 'react-intl'
import en from 'react-intl/locale-data/en'

class App extends Component {

    constructor(props, context) {
        super(props, context);

        this.state = {
            user: null,
            messages: null,
            locale: null,
        };
    }

    componentDidMount() {}

    render() {
        addLocaleData([...en])

        return(
            <IntlProvider locale="en">
                {this.props.children}
            </IntlProvider>
        );
    }
}

export default App