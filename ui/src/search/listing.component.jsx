import React, {Component} from 'react'
import {FormattedRelative} from 'react-intl'
import {
    Link,
} from 'react-router-dom'
import ApiClient from '../common/ApiClient.jsx';

class Search extends Component {

    constructor(props, context) {
        super(props, context);
        this.client = new ApiClient()
        this.state = {
            shows: [],
            integrationKey: 'tv-maze',
            keywords: '',
            loading: false,
        }
        this.handleFormChange = this.handleFormChange.bind(this)
        this.search = this.search.bind(this)
    }

    search(event) {
        event.preventDefault()
        this.setState({loading: true})
        this.client.get('/tv-shows/'+ this.state.integrationKey + '/search?q=' + this.state.keywords).then(res => {
            this.setState({
                shows: res.data || [],
                loading: false,
            })
        })
    }

    handleFormChange(event) {
        const target = event.target;
        const value = target.type === 'checkbox' ? target.checked : target.value;
        const name = target.name;

        this.setState({
            [name]: value
        });
    }

    render() {
        return <div className="row ui-has-modules">
            {this.state.modal}
            <div className="col-12 ui-module">
                <div className="row pb-2">
                    <div className="col-12 d-flex">
                        <div className="p-2">
                            <select
                                className="form-control"
                                name="integrationKey"
                                value={this.state.integrationKey}
                                onChange={this.handleFormChange}
                                id="integrationKey">
                                <option value="tv-maze">
                                    TV Maze
                                </option>
                            </select>
                        </div>
                        <div className="p-2 flex-grow-1">
                            <input
                                type="text"
                                autoComplete="off"
                                placeholder={"Search for a TV Show..."}
                                value={this.state.keywords}
                                className="form-control"
                                onChange={this.handleFormChange}
                                name="keywords"/>
                        </div>
                        <div className="p-2">
                            <button className={'btn btn-primary px-4'} onClick={this.search}>
                                <i className="material-icons mr-1">search</i>
                                Search
                            </button>
                        </div>
                    </div>
                </div>
                <div className="row">
                    {!this.state.loading ? <div className="col-12">
                        {this.state.shows.length > 0 ? <table className="table table-striped">
                            <thead>
                                <tr>
                                    <th>Image</th>
                                    <th>Name</th>
                                    <th>Language</th>
                                    <th style={{width: '65%'}}>Summary</th>
                                </tr>
                            </thead>
                            <tbody>
                                {this.state.shows.map((show, key) => {
                                    return (
                                        <tr key={key}>
                                            <td>{show.imageLink ? <img src={show.imageLink} width={'80px'}/> : <i className="material-icons mr-1">image</i>}</td>
                                            <td>
                                                <a href={show.url} target={'_blank'}>
                                                    {show.name}
                                                </a>
                                            </td>
                                            <td>{show.language}</td>
                                            <td dangerouslySetInnerHTML={{__html: show.summary}} />
                                        </tr>
                                    )
                                })}
                            </tbody>
                        </table> : <p className="text-center">No result...</p>}
                    </div> : <div className="col-12 text-center">
                        <i className="material-icons mr-1">update</i> Loading...
                    </div>}
                </div>
            </div>
        </div>
    }
}

export default Search