import React, {Component} from 'react'
import {
    Router,
    Route,
    Link
} from 'react-router-dom'

import AppRoutes from '../routes.jsx';

const history = require("history").createBrowserHistory()

class Main extends Component {

    constructor(props, context) {
        super(props, context);
    }

    render() {
        return(
            <Router history={history}>
                <div id="main-wrapper">
                    <div className="page-wrapper">
                        <header className="top-bar-nav-container">
                            <div>
                                <div className="navbar-logo fixed-top">
                                    <h3 className="title-logo"><i className="material-icons">movie</i> TV Shows</h3>
                                </div>
                                <nav className="top-bar-nav navbar navbar-expand-md navbar-dark fixed-top">
                                    <div className="col-12">
                                        <div className="collapse navbar-collapse navbar-menu" id="navbarCollapse">
                                            <ul className="navbar-nav mr-auto">
                                                <li className="nav-item menu menu-products menu-active">
                                                    <Link to="/" className="menu waves-effect">
                                                        <i className="material-icons">search</i>
                                                        <span>Search</span>
                                                    </Link>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </nav>
                            </div>


                        </header>
                        {AppRoutes.map((route, index) => (
                            <div key={index} className="container main-container">
                                    <Route
                                        key={index}
                                        path={route.path}
                                        exact={route.exact}
                                        component={route.main}
                                    />
                            </div>
                        ))}
                        <footer className="footer text-center">
                            ©2020 Atelier35.ca
                        </footer>
                    </div>
                </div>
            </Router>
        );
    }
}

export default Main