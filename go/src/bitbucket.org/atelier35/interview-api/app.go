package main

import (
	"bitbucket.org/atelier35/interview-api/app/core"
	"bitbucket.org/atelier35/interview-api/app/integration"
	"bitbucket.org/atelier35/interview-api/app/tvshow"
	"encoding/json"
	"fmt"
	"github.com/codegangsta/negroni"
	"github.com/gorilla/mux"
	"github.com/sarulabs/di"
	"net/http"
	"runtime"
	"time"
)

type App struct {
	Router     *mux.Router
	Registries []core.Register
	appContext di.Container
}

func (a *App) Initialize() error {
	return a.initializeRegistries()
}

func (a *App) registerServices() error {
	container, _ := di.NewBuilder()

	a.Registries = []core.Register{
		integration.Registry{},
		tvshow.Registry{},
	}

	for _, registry := range a.Registries {
		registry.ServicesDefinition(container)
	}
	a.appContext = container.Build()
	req, _ := a.appContext.SubContainer()
	defer req.Delete()

	for index, registry := range a.Registries {
		err := registry.Initialize(req)
		if err != nil {
			fmt.Println(err.Error())
			return err
		}
		fmt.Println(fmt.Sprintf(
			"[Init] # %d Initialization of %s component",
			index,
			registry.Name(),
		))
	}
	return nil
}

func (a *App) initializeRegistries() error {

	err := a.registerServices()
	if err != nil {
		return err
	}
	a.Router = mux.NewRouter()

	for _, registry := range a.Registries {
		apiRoutes := registry.ApiRoutes()
		for _, route := range apiRoutes {
			a.addRoute(route.Method, route.Path, core.NewActionControllerHandler(route.Handler, a.appContext))
			fmt.Println(fmt.Sprintf(
				"[API Route] %s - %s %s",
				registry.Name(),
				route.Method,
				route.Path,
			))
		}
	}
	a.addRoute("GET", "/", a.Health)
	fmt.Println("[Public Route] app - GET /")
	return nil
}

func (a *App) addRoute(method string, path string, handlerFunc negroni.HandlerFunc) {
	handler := negroni.New()
	handler.UseFunc(handlerFunc)
	a.Router.Path(path).Handler(handler).Methods(method)
}

func (a App) Health(w http.ResponseWriter, r *http.Request, next http.HandlerFunc) {

	response := map[string]interface{}{
		"status":     "",
		"goroutines": runtime.NumGoroutine(),
	}
	statusName := map[int]string{
		core.HEALTH_OK:      "ok",
		core.HEALTH_WARNING: "warning",
		core.HEALTH_ERROR:   "error",
	}

	generaStatus := core.HEALTH_OK

	req, _ := a.appContext.SubContainer()
	defer req.Delete()
	for _, registry := range a.Registries {
		status, details := registry.Health(req)
		response[registry.Name()] = map[string]interface{}{
			"status":  statusName[status],
			"details": details,
		}
		if status > generaStatus {
			generaStatus = status
		}
	}
	response["status"] = statusName[generaStatus]

	res, _ := json.Marshal(response)

	w.Header().Set("Access-Control-Allow-Origin", "*")
	w.Header().Set("Content-Type", "application/json")
	if generaStatus == core.HEALTH_ERROR {
		w.WriteHeader(http.StatusInternalServerError)
	} else {
		w.WriteHeader(http.StatusOK)
	}
	_, _ = w.Write(res)
}

func (a App) getHttpServer(addr string) *http.Server {
	srv := &http.Server{
		Addr:         addr,
		ReadTimeout:  30 * time.Second,
		WriteTimeout: 5 * time.Minute,
		Handler:      a.Router,
	}
	return srv
}

func (a App) RunHttp(addr string) error {
	fmt.Println("Api start listen on " + addr)
	return a.getHttpServer(addr).ListenAndServe()
}
