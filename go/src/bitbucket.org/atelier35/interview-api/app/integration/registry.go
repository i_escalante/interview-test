package integration

import (
	"bitbucket.org/atelier35/interview-api/app/core"
	"bitbucket.org/atelier35/interview-api/app/core/errors"
	"github.com/sarulabs/di"
)

type Registry struct{}

func (r Registry) ServicesDefinition(container *di.Builder) {
	_ = container.Add(di.Def{
		Name:  "integration-service",
		Scope: di.Request,
		Build: func(ctx di.Container) (interface{}, error) {
			return NewIntegrator(), nil
		},
	})
}

func (r Registry) Name() string {
	return "integration"
}

func (r Registry) Initialize(container di.Container) errors.DetailedError { return nil }

func (r Registry) ApiRoutes() []core.ApiRouteDefinition {
	return []core.ApiRouteDefinition{}
}

func (r Registry) Health(container di.Container) (int, map[string]string) {
	return core.HEALTH_OK, map[string]string{}
}
