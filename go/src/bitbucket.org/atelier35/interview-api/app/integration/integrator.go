package integration

import (
	"bitbucket.org/atelier35/interview-api/app/core/errors"
	"bitbucket.org/atelier35/interview-api/app/external"
)

type Integrator struct{}

type Service interface {
	SearchTVShows(integrationKey string, keywords string) (shows []external.TVShow, err errors.DetailedError)
}

func NewIntegrator() Service {
	return &Integrator{}
}

func (i Integrator) SearchTVShows(integrationKey string, keywords string) (shows []external.TVShow, err errors.DetailedError) {
	if def, ok := Definitions[integrationKey]; ok {
		service := def.BuildService(def.Configuration)
		if search, ok := service.(TVShowSupport); ok {
			return search.SearchTVShows(keywords)
		}
		return shows, errors.NewSimpleError("This integration does not support tv shows: `%s`", integrationKey)
	}
	return shows, errors.NewSimpleError("This integration does not exist: `%s`", integrationKey)
}
