package tvmaze

import (
	"bitbucket.org/atelier35/interview-api/app/core/errors"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
)

const (
	ApiUrl = "http://api.tvmaze.com%s"
)

type Client struct{}

func NewClient() Client {
	return Client{}
}

func (c Client) GetCollection(url string) (res []map[string]interface{}, err errors.DetailedError) {
	res = make([]map[string]interface{}, 0)
	err = c.Request(http.MethodGet, url, &res)
	if err != nil {
		return res, err
	}
	fmt.Println("res: ", res)
	return res, nil
}

func (c Client) GetItem(url string) (res map[string]interface{}, err errors.DetailedError) {
	res = make(map[string]interface{})
	err = c.Request(http.MethodGet, url, &res)
	if err != nil {
		return res, err
	}
	return res, nil
}

func (c Client) Request(
	method string,
	requestUrl string,
	res interface{},
) (err errors.DetailedError) {
	var request *http.Request
	var e error
	request, e = http.NewRequest(
		method,
		fmt.Sprintf(ApiUrl, requestUrl),
		nil,
	)

	if e != nil {
		return errors.NewSimpleError("TV maze request creation failed : %s", e.Error())
	}
	request.Header.Add("Content-Type", "application/json")

	client := &http.Client{}
	response, e := client.Do(request)
	if e != nil {
		return errors.NewSimpleError("TV maze client request execution failed : %s", e.Error())
	}
	defer response.Body.Close()
	resBytes, e := ioutil.ReadAll(response.Body)
	if e != nil {
		return errors.NewSimpleError("TV maze response malformed : %s", e.Error())
	}

	if method == http.MethodDelete && response.StatusCode == http.StatusOK {
		return nil
	}
	fmt.Println(string(resBytes))
	e = json.Unmarshal(resBytes, &res)
	if e != nil {
		return errors.NewSimpleError("TV maze response malformed : %s", e.Error())
	}
	return nil
}
