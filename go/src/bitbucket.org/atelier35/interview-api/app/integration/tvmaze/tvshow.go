package tvmaze

import "bitbucket.org/atelier35/interview-api/app/core"

type TVShow struct {
	show map[string]interface{}
}

func NewTVShow(show map[string]interface{}) TVShow {
	return TVShow{
		show: core.ConvertInterfaceToMapInterface(show["show"]),
	}
}

func (s TVShow) GetName() string {
	if name, ok := s.show["name"].(string); ok {
		return name
	}
	return ""
}

func (s TVShow) GetLanguage() string {
	if language, ok := s.show["language"].(string); ok {
		return language
	}
	return ""
}

func (s TVShow) GetSummary() string {
	if summary, ok := s.show["summary"].(string); ok {
		return summary
	}
	return ""
}

func (s TVShow) GetImageLink() string {
	image := core.ConvertInterfaceToMapInterface(s.show["image"])
	if original, ok := image["original"].(string); ok {
		return original
	}
	return ""
}

func (s TVShow) GetUrl() string {
	if url, ok := s.show["url"].(string); ok {
		return url
	}
	return ""
}
