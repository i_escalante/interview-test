package integration

import (
	"bitbucket.org/atelier35/interview-api/app/core/errors"
	"bitbucket.org/atelier35/interview-api/app/external"
	"bitbucket.org/atelier35/interview-api/app/integration/tvmaze"
)

const (
	TVMaze = "tv-maze"
)

type Definition struct {
	IntegrationKey string
	Configuration  map[string]string
	BuildService   func(conf map[string]string) interface{}
}

var Definitions = map[string]Definition{
	TVMaze: {
		IntegrationKey: TVMaze,
		Configuration:  map[string]string{},
		BuildService: func(conf map[string]string) interface{} {
			return tvmaze.NewService()
		},
	},
}

type TVShowSupport interface {
	SearchTVShows(keywords string) (shows []external.TVShow, err errors.DetailedError)
}
