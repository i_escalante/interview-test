package external

type TVShow interface {
	GetName() string
	GetLanguage() string
	GetSummary() string
	GetImageLink() string
	GetUrl() string
}
