package errors

import (
	"fmt"
	"net/http"
	"runtime"
	"strconv"
	"time"
)

type DetailedError interface {
	error
	Metadata() map[string]string
	Type() string
	LastCallers() []map[string]string
	ErrorCode() int
}

const (
	TypeNotFound      = "not_found"
	TypeAuthError     = "auth_error"
	TypeInternalError = "not_found"
	TypePaymentError  = "payment_error"
	TypeNoRetryError  = "no_retry"
)

type MetadataError struct {
	message     string
	metadata    map[string]string
	errorType   string
	errorCode   int
	lastCallers []map[string]string
}

func NewSimpleError(format string, args ...interface{}) MetadataError {
	return newMetadataError(fmt.Sprintf(format, args...), "error", 0)
}

func NewNotFoundError(format string, args ...interface{}) MetadataError {
	return newMetadataError(fmt.Sprintf(format, args...), TypeNotFound, http.StatusNotFound)
}

func NewAuthError(message string) MetadataError {
	return newMetadataError(message, TypeAuthError, http.StatusUnauthorized)
}

func NewPaymentError(message string, args ...interface{}) MetadataError {
	return newMetadataError(fmt.Sprintf(message, args...), TypePaymentError, http.StatusPaymentRequired)
}

func NewInternalError(message string) MetadataError {
	return newMetadataError(message, TypeInternalError, http.StatusUnauthorized)
}

func newMetadataError(message string, errorType string, code int) MetadataError {
	err := MetadataError{
		message:   message,
		errorType: errorType,
		errorCode: code,
	}
	var fn string
	var line int
	_, fn, line, _ = runtime.Caller(2)
	err.lastCallers = append(err.lastCallers, map[string]string{
		"fileName":   fn,
		"lineNumber": strconv.Itoa(line),
	})
	_, fn, line, _ = runtime.Caller(3)
	err.lastCallers = append(err.lastCallers, map[string]string{
		"fileName":   fn,
		"lineNumber": strconv.Itoa(line),
	})
	_, fn, line, _ = runtime.Caller(4)
	err.lastCallers = append(err.lastCallers, map[string]string{
		"fileName":   fn,
		"lineNumber": strconv.Itoa(line),
	})
	_, fn, line, _ = runtime.Caller(5)
	err.lastCallers = append(err.lastCallers, map[string]string{
		"fileName":   fn,
		"lineNumber": strconv.Itoa(line),
	})
	_, fn, line, _ = runtime.Caller(6)
	err.lastCallers = append(err.lastCallers, map[string]string{
		"fileName":   fn,
		"lineNumber": strconv.Itoa(line),
	})
	_, fn, line, _ = runtime.Caller(7)
	err.lastCallers = append(err.lastCallers, map[string]string{
		"fileName":   fn,
		"lineNumber": strconv.Itoa(line),
	})

	_, fn, line, _ = runtime.Caller(8)
	err.lastCallers = append(err.lastCallers, map[string]string{
		"fileName":   fn,
		"lineNumber": strconv.Itoa(line),
	})
	_, fn, line, _ = runtime.Caller(9)
	err.lastCallers = append(err.lastCallers, map[string]string{
		"fileName":   fn,
		"lineNumber": strconv.Itoa(line),
	})
	_, fn, line, _ = runtime.Caller(10)
	err.lastCallers = append(err.lastCallers, map[string]string{
		"fileName":   fn,
		"lineNumber": strconv.Itoa(line),
	})
	_, fn, line, _ = runtime.Caller(11)
	err.lastCallers = append(err.lastCallers, map[string]string{
		"fileName":   fn,
		"lineNumber": strconv.Itoa(line),
	})
	_, fn, line, _ = runtime.Caller(12)
	err.lastCallers = append(err.lastCallers, map[string]string{
		"fileName":   fn,
		"lineNumber": strconv.Itoa(line),
	})
	return err
}

func (me MetadataError) Metadata() map[string]string {
	return me.metadata
}

func (me MetadataError) Type() string {
	return me.errorType
}

func (me MetadataError) Error() string {
	return me.message
}

func (me MetadataError) LastCallers() []map[string]string {
	return me.lastCallers
}

func (me MetadataError) ErrorCode() int {
	return me.errorCode
}

type RetryLaterError struct {
	MetadataError
	Delay time.Duration
}

func NewRetryLaterError(delay time.Duration, format string, args ...interface{}) RetryLaterError {
	return RetryLaterError{
		MetadataError: NewSimpleError(format, args),
		Delay:         delay,
	}
}

func NewNoRetryError(message string, args ...interface{}) RetryLaterError {
	return RetryLaterError{
		MetadataError: newMetadataError(fmt.Sprintf(message, args...), TypeNoRetryError, http.StatusUnprocessableEntity),
	}
}

func NewNoRetryErrorFromExistingError(err DetailedError) RetryLaterError {
	return RetryLaterError{
		MetadataError: newMetadataError(err.Error(), TypeNoRetryError, http.StatusUnprocessableEntity),
	}
}
