package core

import (
	"bitbucket.org/atelier35/interview-api/app/core/errors"
	"encoding/json"
	"net/http"
	"reflect"
	"strconv"
)

type ApiResponseType string

const (
	ApiResponseTypeItem       ApiResponseType = "item"
	ApiResponseTypeCollection ApiResponseType = "collection"
	ApiResponseTypeError      ApiResponseType = "error"
)

type ApiResponse struct {
	Type     ApiResponseType
	Status   int
	Body     interface{}
	Metadata interface{}
	Written  []byte
}

func NewItemApiResponse(item interface{}, status int) ApiResponse {
	return ApiResponse{
		Type:   ApiResponseTypeItem,
		Body:   item,
		Status: status,
	}
}

func NewCollectionApiResponse(collection interface{}, status int) ApiResponse {
	return ApiResponse{
		Type:   ApiResponseTypeCollection,
		Body:   collection,
		Status: status,
	}
}

func NewErrorApiResponse(error errors.DetailedError, status int) ApiResponse {
	errorCode := status
	if error.ErrorCode() > 0 {
		errorCode = error.ErrorCode()
	}
	return ApiResponse{
		Type:   ApiResponseTypeError,
		Body:   error,
		Status: errorCode,
	}
}

func (ar *ApiResponse) GetJson() string {
	return string(ar.format())
}

func (ar *ApiResponse) Write(w http.ResponseWriter, r *http.Request) {
	WriteAllowOriginHeader(w, r)
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(ar.Status)
	ar.Written = ar.format()
	w.Write(ar.Written)
}

func (ar ApiResponse) format() []byte {
	var body interface{}
	switch ar.Type {
	case ApiResponseTypeItem:
		body = ar.Body
	case ApiResponseTypeCollection:
		s := reflect.ValueOf(ar.Body)
		if ar.Body == nil || s.Len() == 0 {
			body = map[string]interface{}{
				"data":     make([]string, 0),
				"metadata": ar.Metadata,
			}
		} else {
			body = map[string]interface{}{
				"data":     ar.Body,
				"metadata": ar.Metadata,
			}
		}
	case ApiResponseTypeError:
		err := ar.Body.(errors.DetailedError)
		res := make(map[string]interface{})
		res["error"] = map[string]interface{}{
			"message": err.Error(),
			"type":    err.Type(),
			"code":    strconv.Itoa(ar.Status),
		}
		res["metadata"] = err.Metadata()
		if GetEnvironment() != EnvironmentProduction {
			res["debug"] = err.LastCallers()
		}
		body = res
	}
	res, _ := json.Marshal(body)
	return res
}

func (ar ApiResponse) GetStatus() int {
	return ar.Status
}

func (ar ApiResponse) StringBody() string {
	response, _ := json.Marshal(ar.Body)
	return string(response[:])
}
